package com.b2camp.teamc.parameterservice.dtos.responses;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MCoaResponse {
    private Long id;
    private String coaCode;
    private String coaName;
    private String description;
}
