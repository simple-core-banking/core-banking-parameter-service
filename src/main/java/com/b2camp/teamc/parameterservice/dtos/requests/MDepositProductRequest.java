package com.b2camp.teamc.parameterservice.dtos.requests;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class MDepositProductRequest {

    private String name;
    private Double interestRate;

    @JsonIgnore
    private String code ;
}
