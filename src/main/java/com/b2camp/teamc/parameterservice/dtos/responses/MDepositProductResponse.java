package com.b2camp.teamc.parameterservice.dtos.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class MDepositProductResponse {


    private String id;
    private String name;
    private Double interestRate;
    private String code ;

}
