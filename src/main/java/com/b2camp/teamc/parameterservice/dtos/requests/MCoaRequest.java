package com.b2camp.teamc.parameterservice.dtos.requests;

import lombok.Data;

@Data
public class MCoaRequest {
    private String coaCode;
    private String coaName;
    private String description;
}
