package com.b2camp.teamc.parameterservice.dtos.responses;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MCifWorkResponse {
    private String nameMCif;
    private String idCardMCif;
    private String noTeleponMCif;
    private String emailMCif;
    private String typeMCif;
    private String name;
    private String address;
    private Double penghasilan;

}
