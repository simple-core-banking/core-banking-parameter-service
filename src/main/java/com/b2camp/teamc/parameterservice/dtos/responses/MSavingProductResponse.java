package com.b2camp.teamc.parameterservice.dtos.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MSavingProductResponse {

    private Long id;
    private String productName;
    private String description;
    private String coaCodeMCoa;


}
