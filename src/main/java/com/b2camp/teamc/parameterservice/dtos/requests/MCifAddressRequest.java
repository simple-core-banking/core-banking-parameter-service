package com.b2camp.teamc.parameterservice.dtos.requests;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class MCifAddressRequest {
    @JsonIgnore
    private Long id;
    private String address;
}
