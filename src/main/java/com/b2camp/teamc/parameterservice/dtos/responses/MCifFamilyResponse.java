package com.b2camp.teamc.parameterservice.dtos.responses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MCifFamilyResponse {
    private String nameMCif;
    private String idCardMCif;
    private String noTeleponMCif;
    private String emailMCif;
    private String typeMCif;
    private String familyName;
    private String familyType;
}
