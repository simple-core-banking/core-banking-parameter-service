package com.b2camp.teamc.parameterservice.dtos.responses;

import lombok.*;

@Data
@AllArgsConstructor
@Builder
public class RStatusResponse {
    private Long id;
    private String code;
    private String name;


}
