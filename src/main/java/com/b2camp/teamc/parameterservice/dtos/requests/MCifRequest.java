package com.b2camp.teamc.parameterservice.dtos.requests;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;

@Data
public class MCifRequest {
    @JsonIgnore
    private Long id;
    private String idCard;
    private String name;
    private String npwp;
    private String noTelepon;
    private String email;
    private String type;
}
