package com.b2camp.teamc.parameterservice.dtos.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class MTransactionCodeResponse {
    private Long id;
    private String transactionCode;
    private String transactionName;
}
