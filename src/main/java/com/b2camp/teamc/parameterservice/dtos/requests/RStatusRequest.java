package com.b2camp.teamc.parameterservice.dtos.requests;

import lombok.Data;


@Data
public class RStatusRequest {
    private String code;
    private String name;
}
