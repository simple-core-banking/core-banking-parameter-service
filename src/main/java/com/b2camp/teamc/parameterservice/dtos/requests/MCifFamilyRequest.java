package com.b2camp.teamc.parameterservice.dtos.requests;

import lombok.Data;

@Data
public class MCifFamilyRequest {
    private String familyName;
    private String familyType;
}
