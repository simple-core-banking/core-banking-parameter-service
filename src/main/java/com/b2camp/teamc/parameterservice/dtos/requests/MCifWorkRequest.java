package com.b2camp.teamc.parameterservice.dtos.requests;


import lombok.Data;

@Data
public class MCifWorkRequest {

    private String name;
    private String address;
    private Double penghasilan;

}
