package com.b2camp.teamc.parameterservice.dtos.requests;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MTransactionCodeRequest {
    private String transactionCode;
    private String transactionName;
}
