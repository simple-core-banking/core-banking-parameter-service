package com.b2camp.teamc.parameterservice.dtos.requests;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MSavingProductRequest {
    @JsonIgnore
    private Long id;
    private String productName;
    private String description;
    private Long coaId;


}
