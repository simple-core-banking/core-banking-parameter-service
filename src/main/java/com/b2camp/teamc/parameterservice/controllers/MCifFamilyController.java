package com.b2camp.teamc.parameterservice.controllers;

import com.b2camp.teamc.parameterservice.dtos.requests.MCifFamilyRequest;
import com.b2camp.teamc.parameterservice.services.MCifFamilyService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:8080")
public class MCifFamilyController {

    @Autowired
    private MCifFamilyService service;

    @PostMapping("/mcifFamily/create")
    public Map<String, Object> create(@RequestParam Long id, @RequestBody MCifFamilyRequest mCifFamilyRequest) {
        return service.create(id, mCifFamilyRequest);
    }

    @GetMapping("/mcifFamily/getAll")
    public Map<String, Object> getAll() {
        return service.getAll();
    }

    @GetMapping("/mcifFamily/getById")
    public Map<String, Object> getById(@RequestParam("id") Long id) {
        return service.getById(id);
    }

    @PutMapping("/mcifFamily/update/{id}")
    public Map<String, Object> update(@PathVariable("id") Long id, @RequestBody MCifFamilyRequest mCifFamilyRequest) {
        return service.update(id, mCifFamilyRequest);
    }

    @DeleteMapping("/mcifFamily/delete")
    public Map<String, Object> delete(@RequestParam("id") Long id) {
        return service.delete(id);
    }

}
