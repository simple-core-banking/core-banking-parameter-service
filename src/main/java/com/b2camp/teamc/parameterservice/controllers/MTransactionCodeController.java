package com.b2camp.teamc.parameterservice.controllers;

import com.b2camp.teamc.parameterservice.dtos.requests.MTransactionCodeRequest;
import com.b2camp.teamc.parameterservice.dtos.responses.MTransactionCodeResponse;
import com.b2camp.teamc.parameterservice.services.MTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:8080")
public class MTransactionCodeController {

    @Autowired
    MTransactionService service;

    @PostMapping("/transactioncode/create")
    MTransactionCodeResponse create(@RequestBody MTransactionCodeRequest request) {
        return service.create(request);
    }

    @GetMapping("/transactioncode/findAll")
    List<MTransactionCodeResponse> findAll() {
        return service.findAll();
    }

    @GetMapping("/transactioncode/findById/{id}")
    MTransactionCodeResponse findById(@PathVariable("id") Long id) {
        return service.findById(id);
    }

    @DeleteMapping("/transactioncode/delete/{id}")
    void softDelete(@PathVariable("id") Long id) {
        service.softDelete(id);
    }

    @PutMapping("/transactioncode/update/{id}")
    MTransactionCodeResponse update(@PathVariable("id") Long id, @RequestBody MTransactionCodeRequest request) {
        return service.update(id, request);
    }
}
