package com.b2camp.teamc.parameterservice.controllers;

import com.b2camp.teamc.parameterservice.dtos.requests.MCoaRequest;
import com.b2camp.teamc.parameterservice.dtos.responses.MCoaResponse;
import com.b2camp.teamc.parameterservice.models.MCoa;
import com.b2camp.teamc.parameterservice.services.MCoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:8080")
public class MCoaController {

    @Autowired
    private MCoaService coaService;

    @PostMapping("/coa/create")
    public MCoaResponse create(@RequestBody MCoaRequest mCoaRequest) {
        return coaService.createCoa(mCoaRequest);
    }

    @PutMapping(value = "/coa/update/{id}")
    public MCoaResponse update(@PathVariable(value = "id") Long id, @RequestBody MCoaRequest mCoaRequest) {
        return coaService.updateCoa(id, mCoaRequest);
    }

    @DeleteMapping("/coa/delete/{id}")
    public String deleteCoa(@PathVariable("id") Long id) {
        coaService.deleteCoa(id);
        return "Delete Success";
    }

    @GetMapping("/coa/getById/{id}")
    public MCoaResponse findCoaId(@PathVariable Long id) {
        return coaService.getCoaById(id);
    }

    @GetMapping("/coa/findAll")
    public Iterable<MCoa> findAll() {
        return coaService.getAllCoa();
    }
}
