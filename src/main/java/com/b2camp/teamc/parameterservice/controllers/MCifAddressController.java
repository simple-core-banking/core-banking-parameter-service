package com.b2camp.teamc.parameterservice.controllers;

import com.b2camp.teamc.parameterservice.dtos.requests.MCifAddressRequest;
import com.b2camp.teamc.parameterservice.services.MCifAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api")

public class MCifAddressController {

    @Autowired
    private MCifAddressService service;

    @PostMapping("/mcifAddress/create")
    @CrossOrigin(origins = "http://localhost:8080")
    public Map<String, Object> create(@RequestParam("cif_id") Long id,
            @RequestBody MCifAddressRequest mCifAddressRequest) {
        return service.create(id, mCifAddressRequest);
    }

    @GetMapping("/mcifAddress/getAll")
    @CrossOrigin(origins = "http://localhost:8080")
    public Map<String, Object> getAll() {
        return service.getAll();
    }

    @GetMapping("/mcifAddress/getById")
    @CrossOrigin(origins = "http://localhost:8080")
    public Map<String, Object> getById(@RequestParam("id") Long id) {
        return service.getById(id);
    }

    @PutMapping("/mcifAddress/update/{id}")
    @CrossOrigin(origins = "http://localhost:8080")
    public Map<String, Object> update(@PathVariable("id") Long id, @RequestBody MCifAddressRequest mCifAddressRequest) {
        return service.update(id, mCifAddressRequest);
    }

    @DeleteMapping("/mcifAddress/delete/{id}")
    @CrossOrigin(origins = "http://localhost:8080")
    public Map<String, Object> delete(@PathVariable("id") Long id) {
        return service.delete(id);
    }

}
