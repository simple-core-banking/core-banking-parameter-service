package com.b2camp.teamc.parameterservice.controllers;

import com.b2camp.teamc.parameterservice.dtos.requests.RStatusRequest;
import com.b2camp.teamc.parameterservice.dtos.responses.RStatusResponse;
import com.b2camp.teamc.parameterservice.services.RStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:8080")
public class RStatusController {
    @Autowired
    private RStatusService service;

    @PostMapping("/status/create")
    public ResponseEntity<Object> post(@RequestBody RStatusRequest request) {
        if (service.validation(request.getCode(), request.getName()).isEmpty()) {
            return new ResponseEntity<>(service.add(request), HttpStatus.CREATED);
        }
        return new ResponseEntity<>("Code or Name exists", HttpStatus.BAD_REQUEST);
    }

    @PutMapping("/status/update")
    public ResponseEntity<Object> put(@RequestParam(name = "id") Long id, @RequestBody RStatusRequest request) {
        if (!service.dataCheck(id)) {
            return new ResponseEntity<>("Data Not Found", HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(service.update(id, request), HttpStatus.OK);
    }

    @DeleteMapping("/status/delete")
    public ResponseEntity<Object> softDelete(@RequestParam(name = "id") Long id) {
        if (!service.dataCheck(id)) {
            return new ResponseEntity<>("Data Not Found", HttpStatus.NOT_FOUND);
        }
        service.softDelete(id);
        return new ResponseEntity<>("Data Deleted", HttpStatus.OK);
    }

    @GetMapping("/status/findAll")
    public List<RStatusResponse> findAll() {
        return service.getAll();
    }

    @GetMapping("/status/findById")
    public ResponseEntity<Object> findById(@RequestParam("by") Long id) {
        if (!service.dataCheck(id)) {
            return new ResponseEntity<>("Data Not Found", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(service.getById(id), HttpStatus.OK);
    }

}
