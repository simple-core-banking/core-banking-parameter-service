package com.b2camp.teamc.parameterservice.controllers;

import com.b2camp.teamc.parameterservice.dtos.requests.MSavingProductRequest;
import com.b2camp.teamc.parameterservice.dtos.responses.MSavingProductResponse;
import com.b2camp.teamc.parameterservice.models.MSavingProduct;
import com.b2camp.teamc.parameterservice.services.MSavingProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.function.EntityResponse;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:8080")
public class MSavingProductController {

    @Autowired
    MSavingProductService mSavingProductService;

    @PostMapping("/msavingproduct/create")
    public ResponseEntity<Object> create(@RequestBody MSavingProductRequest mSavingProductRequest) {
        return new ResponseEntity<>(mSavingProductService.createMSProduct(mSavingProductRequest), HttpStatus.CREATED);
    }

    @PutMapping("/msavingproduct/update")
    public ResponseEntity<Object> update(@RequestParam Long id,
            @RequestBody MSavingProductRequest mSavingProductRequest) {
        return new ResponseEntity<>(mSavingProductService.updateProductName(id, mSavingProductRequest), HttpStatus.OK);
    }

    @DeleteMapping("/msavingproduct/delete/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") Long id) {
        mSavingProductService.deleteMSavingProductById(id);
        return new ResponseEntity<>("Data with id number : " + id + " has been deleted", HttpStatus.OK);
    }

    @GetMapping("/msavingproduct/findAll")
    public List<MSavingProductResponse> findAll() {
        return mSavingProductService.getAllSavingProduct();
    }

    @GetMapping("/msavingproduct/findById/{/id}")
    public ResponseEntity<Object> findById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(mSavingProductService.getSavingProductById(id), HttpStatus.ACCEPTED);
    }
}
