package com.b2camp.teamc.parameterservice.controllers;

import com.b2camp.teamc.parameterservice.dtos.requests.MCifWorkRequest;
import com.b2camp.teamc.parameterservice.services.MCifWorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api")
public class MCifWorkController {
    @Autowired
    private MCifWorkService service;

    @PostMapping("/mcifWork/create")
    @CrossOrigin(origins = "http://localhost:8080")
    public Map<String, Object> create(@RequestParam Long id, @RequestBody MCifWorkRequest mCifWorkRequest) {
        return service.create(id, mCifWorkRequest);
    }

    @GetMapping("/mcifWork/getAll")
    @CrossOrigin(origins = "http://localhost:8080")
    public Map<String, Object> getAll() {
        return service.getAll();
    }

    @GetMapping("/mcifWork/getById")
    @CrossOrigin(origins = "http://localhost:8080")
    public Map<String, Object> getById(@RequestParam("id") Long id) {
        return service.getById(id);
    }

    @PutMapping("/mcifWork/update/{id}")
    @CrossOrigin(origins = "http://localhost:8080")
    public Map<String, Object> update(@PathVariable("id") Long id, @RequestBody MCifWorkRequest mCifWorkRequest) {
        return service.update(id, mCifWorkRequest);
    }

    @DeleteMapping("/mcifWork/delete")
    @CrossOrigin(origins = "http://localhost:8080")
    public Map<String, Object> delete(@RequestParam("id") Long id) {
        return service.delete(id);
    }

}
