package com.b2camp.teamc.parameterservice.controllers;

import com.b2camp.teamc.parameterservice.dtos.requests.MDepositProductRequest;
import com.b2camp.teamc.parameterservice.dtos.responses.MDepositProductResponse;
import com.b2camp.teamc.parameterservice.services.MDepositProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins = "http://localhost:8080")
public class MDepositProductController {

    @Autowired
    private MDepositProductService depositProductService;

    @GetMapping("/deposit/findAll") // (value = "/{id}")
    public Iterable<MDepositProductResponse> getAllDepositNo() {
        return depositProductService.getAllDepositNo();
    }

    @GetMapping(value = "/deposit/find/{id}")
    public MDepositProductResponse findDepositById(@PathVariable String id) {
        return depositProductService.getDepositById(id);
    }

    @PostMapping("/deposit/create")
    @ResponseStatus(HttpStatus.CREATED)
    public MDepositProductResponse createDepositNo(@RequestBody MDepositProductRequest depositProductRequest) {
        return depositProductService.createDepositNo(depositProductRequest);
    }

    @DeleteMapping(value = "/deposit/delete/{id}")
    public String delete(@PathVariable String id) {
        depositProductService.deleteDepositProductById(id);
        return "deposit ID deleted";

    }

    @DeleteMapping("/deposit/delete")
    public void deleteAllDeposit() {
        depositProductService.deleteAllDepositNo();
    }

    @PutMapping(value = "/deposit/update/{id}")
    public MDepositProductResponse update(@PathVariable(value = "id") String id,
            @RequestBody MDepositProductRequest mDepositProductRequest) {
        return depositProductService.updateDeposit(id, mDepositProductRequest);
    }
}
