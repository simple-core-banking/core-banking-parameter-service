package com.b2camp.teamc.parameterservice.controllers;

import com.b2camp.teamc.parameterservice.dtos.requests.MCifRequest;
import com.b2camp.teamc.parameterservice.dtos.responses.MCifResponse;
import com.b2camp.teamc.parameterservice.models.MCif;
import com.b2camp.teamc.parameterservice.services.MCifService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:8080")
public class MCifController {
    @Autowired
    private MCifService mCifService;

    @GetMapping("/cif/{idCard}")
    public Optional<MCifResponse> findById(@PathVariable("idCard") String idCard) {
        return mCifService.showCIFByIdCard(idCard);
    }

    @GetMapping("/cif/findAll")
    List<MCifResponse> findAll() {
        return mCifService.findAll();
    }

    @PostMapping("/cif/create")
    public ResponseEntity<Object> insert(@RequestBody MCifRequest request) {
        return new ResponseEntity<>(mCifService.addNewCustomer(request), HttpStatus.OK);
    }

    @PutMapping("/cif/update/{id}")
    public ResponseEntity<Object> updateExistedCustomer(@PathVariable("id") Long id, @RequestBody MCifRequest request) {
        return new ResponseEntity<>(mCifService.updateExistedCustomer(id, request), HttpStatus.OK);
    }

    @DeleteMapping("/cif/delete/{idCard}")
    public void delete(@PathVariable("idCard") String idCard) {
        mCifService.delete(idCard);
    }

}
