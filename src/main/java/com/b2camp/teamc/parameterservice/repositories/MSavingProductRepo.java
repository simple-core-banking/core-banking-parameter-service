package com.b2camp.teamc.parameterservice.repositories;

import com.b2camp.teamc.parameterservice.models.MSavingProduct;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MSavingProductRepo extends JpaRepository<MSavingProduct,Long> {

}
