package com.b2camp.teamc.parameterservice.repositories;


import com.b2camp.teamc.parameterservice.models.MCifAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MCifAddressRepo extends JpaRepository<MCifAddress, Long> {

    Optional<MCifAddress> findByIdAndDeletedFalse(Long id);

    @Query(value="SELECT m_cif from m_cif_address mca" +
            "WHERE mca.id = :id",nativeQuery = true)
    Long getMCifId(@Param("id") Long id);

}
