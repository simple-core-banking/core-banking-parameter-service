package com.b2camp.teamc.parameterservice.repositories;


import com.b2camp.teamc.parameterservice.models.MTransactionCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MTransactionCodeRepo extends JpaRepository<MTransactionCode, Long> {
}
