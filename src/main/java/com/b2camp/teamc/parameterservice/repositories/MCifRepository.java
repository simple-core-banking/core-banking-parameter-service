package com.b2camp.teamc.parameterservice.repositories;


import com.b2camp.teamc.parameterservice.models.MCif;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MCifRepository extends JpaRepository<MCif,Long> {
    boolean existsByIdCardOrNpwpOrNoTelepon(String idCard, String npwp, String phoneNumber);
    Optional<MCif> findByIdCard(String idCard);
}
