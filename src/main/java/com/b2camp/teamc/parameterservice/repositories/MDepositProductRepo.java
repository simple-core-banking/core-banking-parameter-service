package com.b2camp.teamc.parameterservice.repositories;

import com.b2camp.teamc.parameterservice.models.MDepositProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MDepositProductRepo extends JpaRepository <MDepositProduct,String> {

  Optional<MDepositProduct> findByIdAndDeletedFalse(String id);
}
