package com.b2camp.teamc.parameterservice.repositories;

import com.b2camp.teamc.parameterservice.models.MCoa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Map;

public interface MCoaRepo extends JpaRepository<MCoa,Long> {
}
