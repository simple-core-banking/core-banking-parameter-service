package com.b2camp.teamc.parameterservice.repositories;


import com.b2camp.teamc.parameterservice.models.MCifWork;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface MCifWorkRepo extends JpaRepository<MCifWork, Long> {

    Optional<MCifWork> findByIdAndDeletedFalse( Long id);

    @Query(value="select count(mcif_id) from m_cif_work mcw " +
            "where mcw.mcif_id = :id " +
            "and mcw.is_deleted = false", nativeQuery = true)
    Long countData(@Param("id") Long id);

    @Query(value="SELECT m_cif from m_cif_work mcw" +
            "WHERE mcw.id = :id",nativeQuery = true)
    Long getMCifId(@Param("id") Long id);

    /* berfungsi untuk melakukan query pencarian data dimana
        data yang bersifat soft delete nya false  atau belum dilakukan penghapusn
    * */
}
