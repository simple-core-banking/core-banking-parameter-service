package com.b2camp.teamc.parameterservice.repositories;

import com.b2camp.teamc.parameterservice.models.RStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RStatusRepository extends JpaRepository<RStatus,Long> {
    Optional<RStatus> findByCodeOrName(String code,String name);
    Optional<RStatus> findByIdAndDeletedFalse(Long id);
//    Optional<RStatus> findByIdNotAndCodeOrName(Long id, String code, String name);
}
