package com.b2camp.teamc.parameterservice;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@EnableJpaAuditing
@Configuration
@OpenAPIDefinition
// @EnableEurekaClient
public class ParamaterServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParamaterServiceApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

	// @Bean
	// public WebMvcConfigurer(){
	// return new WebMvcConfigurer(){
	// @Override
	// public void addCorsMapping(CorsRegistry registry){
	// registry.addMapping("/**").allowedMethods("GET", "POST", "PUT",
	// "DELETE").allowedHeaders("*").allowedOrigins("*")
	// }
	// };
	// }

}
