package com.b2camp.teamc.parameterservice.models;

import com.b2camp.teamc.parameterservice.models.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "m_deposit_product")
@SuperBuilder
public class MDepositProduct extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_seq")
    @GenericGenerator(name = "id_seq",strategy = "com.b2camp.teamc.parameterservice.models.base.CodeDepositGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = "initial_value",value = "1"),
                    @org.hibernate.annotations.Parameter(name = "increment_size",value = "1")
            })
    @Column(name = "id", unique = true, nullable = false, length = 50)
    private String id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "interest_rate", nullable = false)
    private Double interestRate;


    @Column(name = "code")
    private String code ;



   /* @Builder
    public void preCreate(){
        this.code = "C" +getId();
    }


    public MDepositProduct (String id, String name, Double interestRate, String code, String createdBy, Timestamp createdOn,String lastModifiedBy,Boolean deleted,Timestamp lastModifiedOn ){
        super(id,name,interestRate,createdBy,createdOn,lastModifiedBy) ;
    }
*/
}
