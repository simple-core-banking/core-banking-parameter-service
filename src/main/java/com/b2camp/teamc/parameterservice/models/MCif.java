package com.b2camp.teamc.parameterservice.models;

import com.b2camp.teamc.parameterservice.models.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;


@Data
@Entity
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "m_cif")
public class MCif extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, length = 50)
    private Long id;

    @Column(name = "id_card", nullable = false, length = 100, unique = true)
    private String idCard;

    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @Column(name = "npwp", length = 100, unique = true)
    private String npwp;

    @Column(name = "no_telepon", nullable = false, length = 100, unique = true)
    private String noTelepon;

    @Column(name = "email", length = 100)
    private String email;

    @Column(nullable = false, length = 100)
    private String type;
}
