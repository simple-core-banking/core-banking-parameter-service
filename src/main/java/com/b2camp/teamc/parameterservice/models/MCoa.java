package com.b2camp.teamc.parameterservice.models;

import com.b2camp.teamc.parameterservice.models.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="m_chart_of_account")
@SuperBuilder
public class MCoa extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",unique = true, nullable = false,length = 50)
    private Long id;

    @Column(name = "coa_code",nullable = false)
    private String coaCode;

    @Column(name = "name",nullable = false)
    private String coaName;

    @Column(name = "description",nullable = false)
    private String description;
}
