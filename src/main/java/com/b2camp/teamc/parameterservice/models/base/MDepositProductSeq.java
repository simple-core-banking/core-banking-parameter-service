package com.b2camp.teamc.parameterservice.models.base;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Data
public class MDepositProductSeq {


    @Id
    @GenericGenerator(name = "deposit_code",strategy = "com.b2camp.teamc.parameterservice.models.base.CodeDepositGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = "initial_value",value = "1"),
                    @org.hibernate.annotations.Parameter(name = "increment_size",value = "1")
            })
    @GeneratedValue(generator = "deposit_code",strategy = GenerationType.SEQUENCE)
    private String code;

}
