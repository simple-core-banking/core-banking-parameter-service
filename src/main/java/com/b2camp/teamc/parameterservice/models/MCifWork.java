package com.b2camp.teamc.parameterservice.models;

import com.b2camp.teamc.parameterservice.models.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "m_cif_work")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class MCifWork extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",unique = true, nullable = false)
    private Long id;

    @Column(name = "name",nullable = false,length = 200)
    private String name;

    @Column(name = "address",nullable = false,length = 200)
    private String address;

    @Column(name = "penghasilan",nullable = false,length = 200)
    private Double penghasilan;

    @ManyToOne(cascade = CascadeType.ALL,targetEntity = MCif.class)
    @JoinColumn(name="mcif_id", referencedColumnName = "id")
    private MCif mCif;

    }
