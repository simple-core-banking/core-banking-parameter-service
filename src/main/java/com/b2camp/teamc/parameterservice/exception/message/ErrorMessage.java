package com.b2camp.teamc.parameterservice.exception.message;

public interface ErrorMessage {
    String getCode();
    String getMessage();
}
