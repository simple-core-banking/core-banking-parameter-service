package com.b2camp.teamc.parameterservice.exception.message;

import com.b2camp.teamc.parameterservice.exception.code.MDepositProductCode;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

@AllArgsConstructor
public class MDepositErrorMessage extends Throwable implements ErrorMessage {

    private static final String M_DEPOSIT_NOT_FOUND = "Deposit tidak ada";
    private static final String M_DEPOSIT_IS_EXISTS = "Deposit mempunyai id yang sama dengan yang lain";
    private static final String M_DEPOSIT_IS_BLANK = "Deposit tidak tercetak kosong";


    @Autowired
    private Map<MDepositProductCode, Object> depositProductCodedata;
    private MDepositProductCode messageCode;

    public MDepositErrorMessage(Map<MDepositProductCode, Object> mDepositIsExists) {
    }

    @Override
    public String getCode() {
        return messageCode.name();
    }

    @Override
    public String getMessage() {
        switch (messageCode) {
            case M_DEPOSIT_NOT_FOUND:
                return errorDepositNotFound();
            case M_DEPOSIT_IS_EXISTS:
                return errorDepositExists();
            case M_DEPOSIT_IS_BLANK:
                return errorDepositBlank();
            default:
                throw new RuntimeException();
        }
    }

    private String errorDepositNotFound() {
        return M_DEPOSIT_NOT_FOUND ;
    }

    private String errorDepositExists() {
        return M_DEPOSIT_IS_EXISTS;
    }

    private String errorDepositBlank() {
        return M_DEPOSIT_IS_BLANK;
    }
}
