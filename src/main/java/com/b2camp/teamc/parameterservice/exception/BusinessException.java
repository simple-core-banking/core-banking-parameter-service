package com.b2camp.teamc.parameterservice.exception;

import com.b2camp.teamc.parameterservice.exception.message.ErrorMessage;

public class BusinessException extends RuntimeException{

    private final ErrorMessage message;

    public BusinessException(ErrorMessage message) {
        super(message.getMessage());
        this.message = message;
    }

    @Override
    public String toString() {
        return message.getCode();
    }
}
