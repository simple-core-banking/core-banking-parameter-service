package com.b2camp.teamc.parameterservice.exception.code;

public enum MDepositProductCode {
    M_DEPOSIT_NOT_FOUND,
    M_DEPOSIT_IS_EXISTS,
    M_DEPOSIT_IS_BLANK;
}
