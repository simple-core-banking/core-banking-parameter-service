package com.b2camp.teamc.parameterservice.services.impl;

import com.b2camp.teamc.parameterservice.dtos.requests.MDepositProductRequest;
import com.b2camp.teamc.parameterservice.dtos.responses.MDepositProductResponse;
import com.b2camp.teamc.parameterservice.exception.BusinessException;
import com.b2camp.teamc.parameterservice.exception.code.MDepositProductCode;
import com.b2camp.teamc.parameterservice.exception.message.MDepositErrorMessage;
import com.b2camp.teamc.parameterservice.models.MDepositProduct;
import com.b2camp.teamc.parameterservice.repositories.MDepositProductRepo;
import com.b2camp.teamc.parameterservice.services.MDepositProductService;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class MDepositProductServiceImpl implements MDepositProductService {


    private MDepositProductRepo depositRepo;
    private ModelMapper modelMapper;

//  private TDepositAccountRepo accountDepositRepo;


    @Override
    public List<MDepositProductResponse> getAllDepositNo() {
        var deposits = depositRepo.findAll().stream().filter
                (mDepositProduct -> mDepositProduct.getDeleted().equals(Boolean.FALSE)).collect(Collectors.toList());

        return deposits.stream().map(mDepositProduct ->
                new MDepositProductResponse(
                        mDepositProduct.getId(),
                        mDepositProduct.getName(),
                        mDepositProduct.getInterestRate(),
                        mDepositProduct.getCode())).collect(Collectors.toList());
    }

    @Override
    public MDepositProductResponse getDepositById(String id) {
        if (depositRepo.findByIdAndDeletedFalse(id).isEmpty()) {
            throw new BusinessException(
                    new MDepositErrorMessage(
                            Map.of(MDepositProductCode.M_DEPOSIT_NOT_FOUND, new Object()),
                            MDepositProductCode.M_DEPOSIT_NOT_FOUND));
        }
        var depoistFindByID = depositRepo.findByIdAndDeletedFalse(id).get();


        return MDepositProductResponse.builder()
                .id(depoistFindByID.getId())
                .name(depoistFindByID.getName())
                .code(depoistFindByID.getCode())
                .interestRate(depoistFindByID.getInterestRate())
                .build();


    }

    @Override
    @Transactional
    public MDepositProductResponse createDepositNo(MDepositProductRequest depositProductRequest) {

    /*
        var createDepositProduct = modelMapper.map(depositProductRequest, MDepositProduct.class);
        if (createDepositProduct.equals(depositProductRequest)) {
            throw new BusinessException(
                    new MDepositErrorMessage(
                            Map.of(MDepositProductCode.M_DEPOSIT_IS_EXISTS, new Object()),
                            MDepositProductCode.M_DEPOSIT_IS_EXISTS));
        }
*/

        MDepositProduct setDeposit = MDepositProduct.builder()
                .name(depositProductRequest.getName())
                .interestRate(depositProductRequest.getInterestRate())

                .deleted(Boolean.FALSE)
                .build();
       MDepositProduct savingData = depositRepo.save(setDeposit);

       MDepositProduct savingCodeData = savingData;
        savingCodeData.setCode("D" +savingData.getId());

       savingCodeData =  depositRepo.save(savingCodeData);

        /*        if (setDeposit.getCode().equals(MDepositProduct.class)) {
            throw new BusinessException(
                    new MDepositErrorMessage(
                            Map.of(MDepositProductCode.M_DEPOSIT_IS_EXISTS, new Object()),
                            MDepositProductCode.M_DEPOSIT_IS_EXISTS));
        }*/

        return MDepositProductResponse.builder()
                .id(savingData.getId())
                .name(savingData.getName())
                .code(savingCodeData.getCode())
                .interestRate(savingData.getInterestRate())
                .build();

//        return sendBalikanDataDeposit;
    }


    @Override
    @SneakyThrows
    public MDepositProductResponse updateDeposit(String id, MDepositProductRequest depositProductRequest) {
        var depositModelEntity = depositRepo.existsById(id);
        if (!depositModelEntity) {
            throw new BusinessException(
                    new MDepositErrorMessage(
                            Map.of(MDepositProductCode.M_DEPOSIT_NOT_FOUND, new Object()),
                            MDepositProductCode.M_DEPOSIT_NOT_FOUND));
        }

        MDepositProduct productUpdate = depositRepo.findById(id).get();

        productUpdate.setName(depositProductRequest.getName());
        productUpdate.setInterestRate(depositProductRequest.getInterestRate());

        MDepositProduct savingProductUpdate = depositRepo.save(productUpdate);

        MDepositProductResponse sendBalikanDataDeposit = MDepositProductResponse.builder()
                .id(savingProductUpdate.getId())
                .name(savingProductUpdate.getName())
                .interestRate(savingProductUpdate.getInterestRate())
                .code(savingProductUpdate.getCode())
                .build();

        return sendBalikanDataDeposit;
    }

    @Override
    @Transactional
    public void deleteDepositProductById(String id) {
        var depositDeleteById = depositRepo.existsById(id);
        if (!depositDeleteById) {
            throw new BusinessException(
                    new MDepositErrorMessage(
                            Map.of(MDepositProductCode.M_DEPOSIT_NOT_FOUND, new Object()),
                            MDepositProductCode.M_DEPOSIT_NOT_FOUND));
        }

        depositRepo.findById(id).get().setDeleted(Boolean.TRUE);

    }

    @Override
    @Transactional
    public void deleteAllDepositNo() {
        var isDepositisNull = depositRepo.findAll();
        if (isDepositisNull.isEmpty()) {
            throw new BusinessException(new MDepositErrorMessage(
                    Map.of(MDepositProductCode.M_DEPOSIT_IS_BLANK, new Object()),
                    MDepositProductCode.M_DEPOSIT_NOT_FOUND));
        }
        depositRepo.deleteAll();
        //depositRepo.setDeleted(Boolean.TRUE); ;
    }
}
