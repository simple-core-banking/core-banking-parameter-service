package com.b2camp.teamc.parameterservice.services.impl;


import com.b2camp.teamc.parameterservice.constant.Constant;
import com.b2camp.teamc.parameterservice.dtos.requests.MCifFamilyRequest;
import com.b2camp.teamc.parameterservice.dtos.responses.MCifFamilyResponse;
import com.b2camp.teamc.parameterservice.models.MCifFamily;
import com.b2camp.teamc.parameterservice.repositories.MCifFamilyRepo;
import com.b2camp.teamc.parameterservice.repositories.MCifRepository;
import com.b2camp.teamc.parameterservice.services.MCifFamilyService;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class McifFamilyServiceImpl implements MCifFamilyService {

    @Autowired
    MCifFamilyRepo repository;

    @Autowired
    MCifRepository mCifRepository;

    @Autowired
    ModelMapper mapper;

    private MCifFamily convertToEntity(MCifFamilyRequest mCifFamilyRequest) {
        return mapper.map(mCifFamilyRequest, MCifFamily.class);
    }

    private MCifFamilyResponse convertToDto(MCifFamily mCifFamily) {
        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
        return mapper.map(mCifFamily, MCifFamilyResponse.class);
    }

    @Override
    public Map<String, Object> create(Long id, MCifFamilyRequest request) {

        Map<String, Object> result = new HashMap<>();
        if (mCifRepository.existsById(id)) {
            MCifFamily mCifFamily = convertToEntity(request);
            mCifFamily.setDeleted(Boolean.FALSE);
            mCifFamily.setMCif(mCifRepository.findById(id).get());

            MCifFamily resultInput = repository.save(mCifFamily);

            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, convertToDto(resultInput));
            return result;
        }
        result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
        result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        return result;
    }


    @Override
    public Map<String, Object> getAll() {
        Map<String, Object> result = new HashMap<>();
        List<MCifFamilyResponse> listData = new ArrayList<>();

        for (MCifFamily dataFamily : repository.findAll()) {
            if (!dataFamily.getDeleted()) {
                MCifFamilyResponse response = convertToDto(dataFamily);
                listData.add(response);
            }
        }
        String message;
        if (!listData.isEmpty()) {
            message = Constant.SUCCESS_STRING;
        } else {
            message = Constant.EMPTY_DATA_STRING;
        }

        result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
        result.put(Constant.MESSAGE_STRING, message);
        result.put(Constant.DATA_STRING, listData);
        result.put(Constant.TOTAL_STRING, listData.size());
        return result;
    }

    @Override
    public Map<String, Object> getById(Long id) {
        Map<String, Object> result = new HashMap<>();
        try {
            var data = repository.findByIdAndDeletedFalse(id);

            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, convertToDto(data.get()));
        } catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }

    @Override
    public Map<String, Object> update(Long id, MCifFamilyRequest request) {
        Map<String, Object> result = new HashMap<>();
        try {
            var data = repository.findByIdAndDeletedFalse(id);
            data.get().setFamilyName(request.getFamilyName());
            data.get().setFamilyType(request.getFamilyType());


            MCifFamily resultInput = repository.save(data.get());

            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, convertToDto(resultInput));
        } catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }

    @Override
    public Map<String, Object> delete(Long id) {
        Map<String, Object> result = new HashMap<>();
        try {
            var data = repository.findByIdAndDeletedFalse(id);
            data.get().setDeleted(Boolean.TRUE);

            MCifFamily resultInput = repository.save(data.get());
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
        } catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }
}
