package com.b2camp.teamc.parameterservice.services.impl;

import com.b2camp.teamc.parameterservice.dtos.requests.MTransactionCodeRequest;
import com.b2camp.teamc.parameterservice.dtos.responses.MTransactionCodeResponse;
import com.b2camp.teamc.parameterservice.models.MTransactionCode;
import com.b2camp.teamc.parameterservice.repositories.MTransactionCodeRepo;
import com.b2camp.teamc.parameterservice.services.MTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MTransactionServiceImpl implements MTransactionService {


    @Autowired  //berfungsi sebagai pembentukan constraktor berArgumen
    MTransactionCodeRepo repository;

    @Transactional
    @Override
    public MTransactionCodeResponse create(MTransactionCodeRequest request) {
        MTransactionCode requestToEntity = MTransactionCode.builder()
                .transactionCode(request.getTransactionCode())
                .transactionName(request.getTransactionName())
                .build();

        MTransactionCode saveToDb = repository.save(requestToEntity);
        MTransactionCodeResponse entityToResponse = MTransactionCodeResponse.builder()
                .id(saveToDb.getId())
                .transactionCode(saveToDb.getTransactionCode())
                .transactionName(saveToDb.getTransactionName())
                .build();
        return entityToResponse;
    }

    @Transactional
    @Override
    public MTransactionCodeResponse update(Long id, MTransactionCodeRequest request) {
        var update = repository.findById(id).get();
        update.setTransactionCode(request.getTransactionCode());
        update.setTransactionName(request.getTransactionName());

        MTransactionCodeResponse entityToResponse = MTransactionCodeResponse.builder()
                .id(update.getId())
                .transactionCode(update.getTransactionCode())
                .transactionName(update.getTransactionName())
                .build();
        return entityToResponse ;
    }

    @Transactional
    @Override
    public void softDelete(Long id) {
        var delete = repository.findById(id).get();
        delete.setDeleted(Boolean.TRUE);
    }

    @Override
    public List<MTransactionCodeResponse> findAll() {
        var hasil = repository.findAll();
        List<MTransactionCodeResponse> response = hasil.stream()
        .map(a -> new MTransactionCodeResponse(a.getId(), a.getTransactionCode(), a.getTransactionName()))
        .collect(Collectors.toList());
        return response;
    }

    @Override
    public MTransactionCodeResponse findById(Long id) {
        var hasil = repository.findById(id).get();
        MTransactionCodeResponse response = MTransactionCodeResponse.builder()
                .id(hasil.getId())
                .transactionCode(hasil.getTransactionCode())
                .transactionName(hasil.getTransactionName())
                .build();
        return response;
    }
}
