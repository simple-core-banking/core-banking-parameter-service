package com.b2camp.teamc.parameterservice.services.impl;

import com.b2camp.teamc.parameterservice.dtos.requests.MSavingProductRequest;
import com.b2camp.teamc.parameterservice.dtos.responses.MSavingProductResponse;
import com.b2camp.teamc.parameterservice.models.MSavingProduct;
import com.b2camp.teamc.parameterservice.repositories.MCoaRepo;
import com.b2camp.teamc.parameterservice.repositories.MSavingProductRepo;
import com.b2camp.teamc.parameterservice.services.MSavingProductService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MSavingProductServiceImpl implements MSavingProductService {
    @Autowired
    private MSavingProductRepo productRepo;
    @Autowired
    private MCoaRepo coaRepo;
    @Autowired
    private ModelMapper mapper;

    private MSavingProduct convertToEntity(MSavingProductRequest request){
        return mapper.map(request,MSavingProduct.class);
    }
    private MSavingProductResponse convertToResponse(MSavingProduct product){
        return mapper.map(product,MSavingProductResponse.class);
    }
    @Transactional
    @Override
    public List<MSavingProductResponse> getAllSavingProduct() {
        var entityData = productRepo.findAll().stream().filter(data -> data.getDeleted().equals(Boolean.FALSE)).collect(Collectors.toList());

        return entityData.stream().map(this::convertToResponse).collect(Collectors.toList());
    }

    @Override
    public Optional<MSavingProductResponse> getSavingProductById(Long id) {
        var checkData = productRepo.findById(id);
        return checkData.map(this::convertToResponse);
    }

    @Transactional
    @Override
    public MSavingProductResponse createMSProduct( MSavingProductRequest savingProductRequest) {
        var getRequest = convertToEntity(savingProductRequest);
                getRequest.setDeleted(Boolean.FALSE);
                getRequest.setMCoa(coaRepo.getById(savingProductRequest.getCoaId()));

        return convertToResponse(productRepo.save(getRequest));
    }

    @Override
    @Transactional
    public Optional<MSavingProductResponse> updateProductName(Long id, MSavingProductRequest savingProductRequest) {

        var checkData = productRepo.findById(id);
         if(checkData.isPresent()){
            var resultValid = inputValid(savingProductRequest,checkData);
           checkData.get().setProductName(resultValid.getProductName());
           checkData.get().setDescription(resultValid.getDescription());
           checkData.get().setMCoa(coaRepo.getById(resultValid.getCoaId()));
            return checkData.map(this::convertToResponse);
         }
         return Optional.empty();
    }

    @Transactional
    @Override
    public void deleteMSavingProductById(Long id) {
            var cek = productRepo.findById(id).get();
            cek.setDeleted(Boolean.TRUE);
            productRepo.save(cek);
    }

    private MSavingProductRequest inputValid(MSavingProductRequest request, Optional<MSavingProduct> checkData){
        return MSavingProductRequest.builder()
                .productName(request.getProductName().equals(null)?checkData.get().getProductName(): request.getProductName())
                .description(request.getDescription().equals(null)?checkData.get().getDescription(): request.getDescription())
                .coaId(request.getCoaId().equals(null)?checkData.get().getMCoa().getId(): request.getCoaId())
                .build();
    }
}
