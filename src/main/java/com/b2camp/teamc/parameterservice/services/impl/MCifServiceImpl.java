package com.b2camp.teamc.parameterservice.services.impl;

import com.b2camp.teamc.parameterservice.dtos.requests.MCifRequest;
import com.b2camp.teamc.parameterservice.dtos.responses.MCifResponse;
import com.b2camp.teamc.parameterservice.models.MCif;
import com.b2camp.teamc.parameterservice.repositories.MCifRepository;
import com.b2camp.teamc.parameterservice.services.MCifService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MCifServiceImpl implements MCifService {
    @Autowired
    private MCifRepository repository;
    @Autowired
    private ModelMapper mapper;

    public MCifResponse convertToResponse(MCif data) {
        return mapper.map(data, MCifResponse.class);
    }

    public MCif convertToEntity(MCifRequest request) {
        return mapper.map(request, MCif.class);
    }

    @Transactional
    @Override
    public MCifResponse addNewCustomer(MCifRequest request) {
        var getRequest = convertToEntity(request);
        getRequest.setDeleted(Boolean.FALSE);
        getRequest.setType(request.getType().toUpperCase());
        return convertToResponse(repository.save(getRequest));
    }

    @Transactional
    @Override
    public MCifResponse updateExistedCustomer(Long id, MCifRequest request) {
        var getRequest = repository.findById(id);
        if (getRequest.isPresent()) {
            getRequest.get().setIdCard(request.getIdCard());
            getRequest.get().setNpwp(request.getNpwp());
            getRequest.get().setEmail(request.getEmail());
            getRequest.get().setName(request.getName());
            getRequest.get().setNoTelepon(request.getNoTelepon());
            getRequest.get().setType(request.getType().toUpperCase());

            var update = repository.save(getRequest.get());

            return convertToResponse(update);

        }
        return convertToResponse(getRequest.orElseThrow());
    }

    @Override
    public void delete(String idCard) {
        var status = repository.findByIdCard(idCard).stream().filter(mCif -> mCif.getDeleted().equals(Boolean.FALSE))
                .findFirst();
        if (status.isPresent()) {
            status.get().setDeleted(Boolean.TRUE);
            repository.save(status.get());
        }
        status.orElseThrow();
    }

    @Override
    public List<MCifResponse> findAll() {
        var entityData = repository.findAll().stream().filter(mCif -> mCif.getDeleted().equals(Boolean.FALSE))
                .collect(Collectors.toList());

        return entityData.stream().map(this::convertToResponse).collect(Collectors.toList());
    }

    @Override
    public Optional<MCifResponse> showCIFByIdCard(String idCard) {
        var data = repository.findByIdCard(idCard);
        return data.map(this::convertToResponse);
    }

}
