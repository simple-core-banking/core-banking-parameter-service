package com.b2camp.teamc.parameterservice.services.impl;

import com.b2camp.teamc.parameterservice.dtos.requests.MSavingProductRequest;
import com.b2camp.teamc.parameterservice.dtos.requests.RStatusRequest;
import com.b2camp.teamc.parameterservice.dtos.responses.MSavingProductResponse;
import com.b2camp.teamc.parameterservice.dtos.responses.RStatusResponse;
import com.b2camp.teamc.parameterservice.models.MSavingProduct;
import com.b2camp.teamc.parameterservice.models.RStatus;
import com.b2camp.teamc.parameterservice.repositories.RStatusRepository;
import com.b2camp.teamc.parameterservice.services.RStatusService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RStatusServiceImpl implements RStatusService {
    @Autowired
    private RStatusRepository repository;

    @Autowired
    private ModelMapper mapper;

    private RStatus convertToEntity(RStatusRequest request){
        return mapper.map(request,RStatus.class);
    }
    private RStatusResponse convertToResponse(RStatus entity){
        return mapper.map(entity,RStatusResponse.class);
    }

    @Transactional
    @Override
    public RStatusResponse add(RStatusRequest request) {
        var setStatus = convertToEntity(request);
        setStatus.setDeleted(Boolean.FALSE);

        return convertToResponse(repository.save(setStatus));
    }


    @Transactional
    @Override
    public RStatusResponse update(Long id, RStatusRequest request) {
        var cek = repository.findById(id);
        if(cek.isEmpty()||cek.get().getDeleted().equals(Boolean.TRUE)){
           throw new NullPointerException();
        }
        cek.get().setCode(request.getCode());
        cek.get().setName(request.getName());

        return convertToResponse(repository.save(cek.get()));
    }

    @Transactional
    @Override
    public void softDelete(Long id) {
        var cek = repository.findById(id);
        if(cek.isEmpty()||cek.get().getDeleted().equals(Boolean.TRUE)){
            throw new NullPointerException();
        }
        cek.get().setDeleted(Boolean.TRUE);

        repository.save(cek.get());
    }

    @Override
    public List<RStatusResponse> getAll() {
        var hasil = repository.findAll().stream()
                .filter(rStatus -> rStatus.getDeleted().equals(Boolean.FALSE)).collect(Collectors.toList());
        return hasil.stream()
                .map(this::convertToResponse)
                .collect(Collectors.toList());
    }

    @Override
    public RStatusResponse getById(Long id) {
                return convertToResponse(repository.findByIdAndDeletedFalse(id).get());
    }

    @Override
    public boolean dataCheck(Long id) {
        return repository.findById(id).stream()
                .anyMatch(rStatus -> rStatus.getDeleted().equals(Boolean.FALSE));

    }

    @Override
    public Optional<RStatus> validation(String code, String name) {
        return repository.findByCodeOrName(code,name);

    }


}
