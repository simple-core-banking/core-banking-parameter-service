package com.b2camp.teamc.parameterservice.services;

import com.b2camp.teamc.parameterservice.dtos.requests.MDepositProductRequest;
import com.b2camp.teamc.parameterservice.dtos.responses.MDepositProductResponse;

import java.util.List;

public interface MDepositProductService {
    List<MDepositProductResponse> getAllDepositNo();
    MDepositProductResponse getDepositById(String id);
    MDepositProductResponse createDepositNo(MDepositProductRequest depositProductRequest);
    MDepositProductResponse updateDeposit(String id,MDepositProductRequest depositProductRequest);
    void deleteDepositProductById(String id);
    void deleteAllDepositNo();

}
