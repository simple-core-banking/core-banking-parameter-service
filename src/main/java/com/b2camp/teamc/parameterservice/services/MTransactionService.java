package com.b2camp.teamc.parameterservice.services;

import com.b2camp.teamc.parameterservice.dtos.requests.MTransactionCodeRequest;
import com.b2camp.teamc.parameterservice.dtos.responses.MTransactionCodeResponse;

import java.util.List;

public interface MTransactionService {
    MTransactionCodeResponse create(MTransactionCodeRequest request);
    MTransactionCodeResponse update(Long id, MTransactionCodeRequest request);
    void softDelete(Long id);
    List<MTransactionCodeResponse> findAll();
    MTransactionCodeResponse findById(Long id);
}
