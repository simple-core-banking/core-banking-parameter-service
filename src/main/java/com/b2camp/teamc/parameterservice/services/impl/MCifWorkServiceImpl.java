package com.b2camp.teamc.parameterservice.services.impl;


import com.b2camp.teamc.parameterservice.constant.Constant;
import com.b2camp.teamc.parameterservice.dtos.requests.MCifWorkRequest;
import com.b2camp.teamc.parameterservice.dtos.responses.MCifWorkResponse;
import com.b2camp.teamc.parameterservice.models.MCifWork;
import com.b2camp.teamc.parameterservice.repositories.MCifRepository;
import com.b2camp.teamc.parameterservice.repositories.MCifWorkRepo;
import com.b2camp.teamc.parameterservice.services.MCifWorkService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MCifWorkServiceImpl implements MCifWorkService {
    @Autowired
    private MCifWorkRepo repository;
    @Autowired
    private MCifRepository mCifRepository;

    @Autowired
    private ModelMapper mapper;

    private MCifWork convertToEntity(MCifWorkRequest mCifWorkRequest) {
        return mapper.map(mCifWorkRequest, MCifWork.class);
    }

    private MCifWorkResponse convertToDto(MCifWork mCifWork) {
        return mapper.map(mCifWork, MCifWorkResponse.class);
    }

    @Override
    public Map<String, Object> create(Long id, MCifWorkRequest request) {
        Map<String, Object> result = new HashMap<>();

            if(repository.countData(id) <=5 && mCifRepository.existsById(id)){
            MCifWork mCifWork = convertToEntity(request);
            mCifWork.setDeleted(Boolean.FALSE);
            mCifWork.setMCif(mCifRepository.findById(id).get());

            MCifWork resultInput = repository.save(mCifWork);

            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, convertToDto(resultInput));
            return result;
            }
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
            return result;
    }


    @Override
    public Map<String, Object> getAll() {
        Map<String, Object> result = new HashMap<>();
        List<MCifWorkResponse> listData = new ArrayList<>();

        for (MCifWork data : repository.findAll()) {
            if (!data.getDeleted()) {
                listData.add(convertToDto(data));
            }
        }
        String message;
        if (!listData.isEmpty()) {
            message = Constant.SUCCESS_STRING;
        }
        else {
            message = Constant.EMPTY_DATA_STRING;
        }

        result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
        result.put(Constant.MESSAGE_STRING, message);
        result.put(Constant.DATA_STRING, listData);
        result.put(Constant.TOTAL_STRING, listData.size());
        return result;
    }

    @Override
    public Map<String, Object> getById(Long id) {
        Map<String, Object> result = new HashMap<>();
        String message;
        try {
            var data = repository.findByIdAndDeletedFalse(id);

            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, convertToDto(data.get()));
        }
        catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }


    @Override
    public Map<String, Object> update(Long id, MCifWorkRequest request) {
        Map<String, Object> result = new HashMap<>();
        try {
            var data = repository.findByIdAndDeletedFalse(id);

            data.get().setAddress(request.getAddress());
            data.get().setName(request.getName());
            data.get().setPenghasilan(request.getPenghasilan());
            MCifWork resultInput = repository.save(data.get());

            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, convertToDto(resultInput));
        }
        catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }

    @Override
    public Map<String, Object> delete(Long id) {
        Map<String, Object> result = new HashMap<>();
        try {
            var data = repository.findByIdAndDeletedFalse(id);
            data.get().setDeleted(Boolean.TRUE);

            MCifWork resultInput = repository.save(data.get());
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
        }
        catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }
}
