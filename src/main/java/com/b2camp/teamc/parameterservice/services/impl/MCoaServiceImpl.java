package com.b2camp.teamc.parameterservice.services.impl;

import com.b2camp.teamc.parameterservice.dtos.requests.MCoaRequest;
import com.b2camp.teamc.parameterservice.dtos.responses.MCoaResponse;
import com.b2camp.teamc.parameterservice.models.MCoa;
import com.b2camp.teamc.parameterservice.repositories.MCoaRepo;
import com.b2camp.teamc.parameterservice.services.MCoaService;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;

@Service
@AllArgsConstructor
public class MCoaServiceImpl implements MCoaService {

    private final MCoaRepo mCoaRepo;

    @Override
    @Transactional

    public List<MCoa> getAllCoa(){
        return mCoaRepo.findAll();
    }

    @Override
    public MCoaResponse getCoaById(Long id) {
        var hasil = mCoaRepo.findById(id).get();
        MCoaResponse response = MCoaResponse.builder()
                .id(hasil.getId())
                .coaCode(hasil.getCoaCode())
                .coaName(hasil.getCoaName())
                .description(hasil.getDescription())
                .build();
        return response;
    }

    @Transactional
    @Override
    public MCoaResponse createCoa(MCoaRequest coaRequest) { //method = butuh output (kiri), input (kanan)
        MCoa mCoa = MCoa.builder()
                .coaCode(coaRequest.getCoaCode())
                .coaName(coaRequest.getCoaName())
                .description(coaRequest.getDescription())
                .build(); //isi method untuk mengubah input menjadi output

        MCoa mCoaSave = mCoaRepo.save(mCoa);

        return MCoaResponse.builder()
                .id(mCoaSave.getId())
                .coaCode(mCoaSave.getCoaCode())
                .coaName(mCoaSave.getCoaName())
                .description(mCoaSave.getDescription())
                .build();

    }

    @Override
    @Transactional
    @SneakyThrows
    public MCoaResponse updateCoa(Long id, MCoaRequest coaRequest) {
        MCoa mCoaUpdate = mCoaRepo.findById(id).get();
        mCoaUpdate.setCoaCode(coaRequest.getCoaCode());
        mCoaUpdate.setCoaName(coaRequest.getCoaName());
        mCoaUpdate.setDescription(coaRequest.getDescription());

        mCoaRepo.save(mCoaUpdate);
        return MCoaResponse.builder()
                .id(mCoaUpdate.getId())
                .coaCode(mCoaUpdate.getCoaCode())
                .coaName(mCoaUpdate.getCoaName())
                .description(mCoaUpdate.getDescription())
                .build();
    }

    @Transactional
    @Override
    public void deleteCoa(Long id) {
        MCoa mCoaDelete = mCoaRepo.findById(id).get();
        mCoaDelete.setDeleted(Boolean.TRUE);

        mCoaRepo.save(mCoaDelete);
    }
}
