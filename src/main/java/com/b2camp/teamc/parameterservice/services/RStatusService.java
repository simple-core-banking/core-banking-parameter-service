package com.b2camp.teamc.parameterservice.services;

import com.b2camp.teamc.parameterservice.dtos.requests.RStatusRequest;
import com.b2camp.teamc.parameterservice.dtos.responses.RStatusResponse;
import com.b2camp.teamc.parameterservice.models.RStatus;

import java.util.List;
import java.util.Optional;

public interface RStatusService {
    RStatusResponse add(RStatusRequest request);

    RStatusResponse update(Long id, RStatusRequest request);

    void softDelete(Long id);

    List<RStatusResponse> getAll();

    RStatusResponse getById(Long id);

    boolean dataCheck(Long id);

    Optional<RStatus> validation(String code, String name);
//    Optional<RStatus> validation2(Long id,String code,String name);
}
