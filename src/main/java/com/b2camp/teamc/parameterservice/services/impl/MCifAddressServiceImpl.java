package com.b2camp.teamc.parameterservice.services.impl;


import com.b2camp.teamc.parameterservice.constant.Constant;
import com.b2camp.teamc.parameterservice.dtos.requests.MCifAddressRequest;
import com.b2camp.teamc.parameterservice.dtos.responses.MCifAddressResponse;
import com.b2camp.teamc.parameterservice.models.MCifAddress;
import com.b2camp.teamc.parameterservice.repositories.MCifAddressRepo;
import com.b2camp.teamc.parameterservice.repositories.MCifRepository;
import com.b2camp.teamc.parameterservice.services.MCifAddressService;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeMap;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class MCifAddressServiceImpl implements MCifAddressService {
    @Autowired
    private MCifAddressRepo repository;

    @Autowired
    private MCifRepository mCifRepository;

    @Autowired
    private ModelMapper mapper;

    private MCifAddress convertToEntity(MCifAddressRequest request){
        return mapper.map(request,MCifAddress.class);
    }

    private MCifAddressResponse convertToResponse(MCifAddress address){
        return mapper.map(address,MCifAddressResponse.class);
    }


    @Override
    public Map<String, Object> create(Long cifId, MCifAddressRequest request) {
        Map<String, Object> result = new HashMap<>();
        if (mCifRepository.existsById(cifId)) {
           var getData = mCifRepository.findById(cifId);
            var setCifAddress = convertToEntity(request);
            setCifAddress.setDeleted(Boolean.FALSE);
            setCifAddress.setMCif(getData.get());
            setCifAddress.setName(getData.get().getName());
            var resultInput = repository.save(setCifAddress);

            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, convertToResponse(resultInput));
            return result;
        }
        result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
        result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        return result;
    }

    @Override
    public Map<String, Object> getAll() {
        Map<String, Object> result = new HashMap<>();
        List<MCifAddressResponse> listData = new ArrayList<>();

        for (MCifAddress data : repository.findAll().stream().sorted(Comparator.comparing(MCifAddress::getName)).collect(Collectors.toList())) {
            if (!data.getDeleted()) {
                 var addressResponses = convertToResponse(data);
                listData.add(addressResponses);
            }
        }
        String message;
        if (!listData.isEmpty()) {
            message = Constant.SUCCESS_STRING;
        } else {
            message = Constant.EMPTY_DATA_STRING;
        }

        result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
        result.put(Constant.MESSAGE_STRING, message);
        result.put(Constant.DATA_STRING, listData);
        result.put(Constant.TOTAL_STRING, listData.size());
        return result;
    }

    @Override
    public Map<String, Object> getById(Long id) {
        Map<String, Object> result = new HashMap<>();
        try {
            var data = repository.findByIdAndDeletedFalse(id);
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, convertToResponse(data.get()));
        } catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }

    //bisa juga digunakan generic class

    @Override
    public Map<String, Object> update(Long id, MCifAddressRequest request) {
        Map<String, Object> result = new HashMap<>();
        try {
            var data = repository.findByIdAndDeletedFalse(id);
            data.get().setAddress(request.getAddress());

            MCifAddress resultInput = repository.save(data.get());

            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, convertToResponse(resultInput));
        } catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }

    @Override
    public Map<String, Object> delete(Long id) {
        Map<String, Object> result = new HashMap<>();
        try {
            var data = repository.findByIdAndDeletedFalse(id);
            data.get().setDeleted(Boolean.TRUE);

            MCifAddress resultInput = repository.save(data.get());
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
        } catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }


}
