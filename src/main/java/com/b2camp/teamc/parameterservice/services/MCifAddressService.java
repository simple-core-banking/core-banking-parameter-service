package com.b2camp.teamc.parameterservice.services;


import com.b2camp.teamc.parameterservice.dtos.requests.MCifAddressRequest;

import java.util.Map;

public interface MCifAddressService {
    Map<String, Object> create(Long cifId, MCifAddressRequest request);
    Map<String, Object> getAll();
    Map<String, Object> getById(Long id);
    Map<String, Object> update(Long id, MCifAddressRequest request);
    Map<String, Object> delete(Long id);

}
