package com.b2camp.teamc.parameterservice.services;

import com.b2camp.teamc.parameterservice.dtos.requests.MCoaRequest;
import com.b2camp.teamc.parameterservice.dtos.responses.MCoaResponse;
import com.b2camp.teamc.parameterservice.models.MCoa;

import java.util.List;

public interface MCoaService {
    Iterable<MCoa> getAllCoa();

    MCoaResponse getCoaById(Long id);

    MCoaResponse createCoa(MCoaRequest coaRequest);

    MCoaResponse updateCoa(Long id, MCoaRequest coaRequest);

    void deleteCoa(Long id);
}
