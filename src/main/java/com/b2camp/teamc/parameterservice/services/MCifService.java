package com.b2camp.teamc.parameterservice.services;


import com.b2camp.teamc.parameterservice.dtos.requests.MCifRequest;
import com.b2camp.teamc.parameterservice.dtos.responses.MCifResponse;

import java.util.List;
import java.util.Optional;

public interface MCifService {
MCifResponse addNewCustomer(MCifRequest request);
MCifResponse updateExistedCustomer(Long id, MCifRequest request);
void delete(String idCard);
List<MCifResponse> findAll();
Optional<MCifResponse> showCIFByIdCard(String idCard);
}
