package com.b2camp.teamc.parameterservice.services;


import com.b2camp.teamc.parameterservice.dtos.requests.MCifFamilyRequest;

import java.util.Map;

public interface MCifFamilyService {
    Map<String, Object> create(Long id, MCifFamilyRequest request);
    Map<String, Object> getAll();
    Map<String, Object> getById(Long id);
    Map<String, Object> update(Long id, MCifFamilyRequest request);
    Map<String, Object> delete(Long id);
}
