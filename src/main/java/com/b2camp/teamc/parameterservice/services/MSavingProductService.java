package com.b2camp.teamc.parameterservice.services;

import com.b2camp.teamc.parameterservice.dtos.requests.MSavingProductRequest;
import com.b2camp.teamc.parameterservice.dtos.responses.MSavingProductResponse;

import java.util.List;
import java.util.Optional;

public interface MSavingProductService {
    List<MSavingProductResponse> getAllSavingProduct();

    Optional<MSavingProductResponse> getSavingProductById(Long id);

    MSavingProductResponse createMSProduct( MSavingProductRequest savingProductRequest);

    Optional<MSavingProductResponse> updateProductName(Long id, MSavingProductRequest savingProductRequest);

    void deleteMSavingProductById(Long id);
}
