package com.b2camp.teamc.parameterservice.services;


import com.b2camp.teamc.parameterservice.dtos.requests.MCifWorkRequest;

import java.util.Map;

public interface MCifWorkService {

    Map<String, Object> create(Long id, MCifWorkRequest request);
    Map<String, Object> getAll();
    Map<String, Object> getById(Long id);
    Map<String, Object> update(Long id, MCifWorkRequest request);
    Map<String, Object> delete(Long id);
}
